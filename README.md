# My Pandoc Templates

A collection of [pandoc] templates that I've found on the internet or that I've made or modified.

1. `italicquotes.tex`
	* This is the default pandoc latex template, `default.latex` modified so that block quotes are italicized.

2. `simple.tex`
	* This is a straightforward latex template, useful for homework or "normal" documents.
	* Found in [mwhite](https://github.com/mwhite)'s [pandoc-templates repository](https://github.com/mwhite/pandoc-templates) on github.
	
3. `modern.tex`
	* This is a nice, modern PDF-ebook style template, with title headers on every page and a number of variables for customization. (Use `pandoc --variable mainfont="Arial"` for example).
	* Found among the [pandoc demos](http://johnmacfarlane.net/pandoc/demos.html) as example 14 and called [mytemplate.tex](http://johnmacfarlane.net/pandoc/demo/mytemplate.tex)
	* I modified this to have italicized block quotes as well.

	
[pandoc]: http://johnmacfarlane.net/pandoc/
